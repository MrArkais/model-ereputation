import json

from config import config as cnf

from preprocess import datasetOp
from preprocess import kerasOp
from preprocess import nltkOp
from preprocess import sklearnOp
from preprocess import w2cOp

from process import modelProcess

from result import analyzeResult

if __name__ == "__main__":
    args = cnf.parseArgs()
    cnf.initialiseConfig(args["config"])
    cnf.initialiseLog()

    cnf.logger.info("Début du traitement")
    datasetOp.getDataset(
        datasetPath = cnf.config["DATASET"]["datasetPath"],
        datasetEncoding = cnf.config["DATASET"]["datasetEncoding"],
        datasetColumns = json.loads(cnf.config["DATASET"]["datasetColumns"])
    )

    datasetOp.datasetDf.text = nltkOp.preprocessDataset(datasetOp.datasetDf)
    trainDf, testDf = sklearnOp.splitTrainTestDf(datasetOp.datasetDf)

    documents = w2cOp.getDocuments(trainDf)
    w2cOp.createModel()
    w2cOp.buildVocab(documents)
    w2cOp.trainW2VModel(documents)

    vocabSize = kerasOp.initTokenizer(trainDf)
    xTrain, xTest = kerasOp.tokenizeText(trainDf, testDf)

    sklearnOp.initEncoder()
    yTrain, yTest = sklearnOp.labelEncoder(trainDf, testDf)

    embeddingMatrix = w2cOp.embeddingMatrix(vocabSize, kerasOp.tokenizer)
    embeddingLayer = kerasOp.embeddingLayer(vocabSize, embeddingMatrix)

    modelProcess.buildModel(embeddingLayer)
    modelProcess.compileModel()
    callbacks = modelProcess.getCallbacks()
    trainResult = modelProcess.trainModel(xTrain, yTrain, callbacks)
    modelProcess.evaluateModel(xTest, yTest)

    modelProcess.saveModel(kerasOp.tokenizer, sklearnOp.encoder)

    analyzeResult.showResultGraph(trainResult)
        
    cnf.logger.info("Fin du traitement")
