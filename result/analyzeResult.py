import matplotlib.pyplot as plt

def showResultGraph(trainResult):
    acc = trainResult.history["accuracy"]
    val_acc = trainResult.history["val_accuracy"]
    loss = trainResult.history["loss"]
    val_loss = trainResult.history["val_loss"]
    
    epochs = range(len(acc))
    
    plt.plot(epochs, acc, 'b', label = "Training accuracy")
    plt.plot(epochs, val_acc, 'r', label = "Validation accuracy")
    plt.title("Training and validation accuracy")
    plt.legend()
    plt.figure()
    
    plt.plot(epochs, loss, 'b', label = "Training loss")
    plt.plot(epochs, val_loss, 'r', label = "Validation loss")
    plt.title("Training and validation loss")
    plt.legend()
    plt.show()