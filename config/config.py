import configparser
import logging
import argparse
from logging.handlers import RotatingFileHandler
from datetime import datetime

config = None
logger = None

def initialiseConfig(pathConfig):
    global config
    config = configparser.ConfigParser()
    config.read(pathConfig)

def initialiseLog():
    global logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    timestampDebut = datetime.now().strftime("%Y%m%d_%H%M%S")
    nomFichierLog = config["LOG"]["pathLog"] + str(timestampDebut) + "_" + config["LOG"]["nameFileLog"]
 
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler = RotatingFileHandler(nomFichierLog, 'w', 100000000, 1)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

def parseArgs():
    parser = argparse.ArgumentParser(description='Text classification Tensorflow model')
    parser.add_argument("-c", "--config", required=True, help="path du fichier de configuration")
    return vars(parser.parse_args())
