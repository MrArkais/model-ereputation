#################################################
### Dataset                                   ###
#################################################

import time
import pandas as pd
from config import config as cnf

datasetDf = None

def getDataset(datasetPath, datasetEncoding, datasetColumns):
    cnf.logger.info("DEBUT : Récupération du dataset")
    startTime = time.time()
    global datasetDf
    datasetDf = pd.read_csv(datasetPath, encoding = datasetEncoding , names = datasetColumns)
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Récupération du dataset")

def getDfLength():
    return len(datasetDf)

