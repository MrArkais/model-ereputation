#################################################
### Scikit-Learn                              ###
#################################################

import time
from decimal import Decimal
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from config import config as cnf

encoder = None

def splitTrainTestDf(datasetDf):
    cnf.logger.info("DEBUT : Split du dataset")
    startTime = time.time()
    partEntrainement = cnf.config["DATASET"]["trainSize"]
    trainDf, testDf = train_test_split(datasetDf, test_size = 0.2, random_state = 42)
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Split du dataset")
    return trainDf, testDf

def initEncoder():
    cnf.logger.info("DEBUT : Initialisation de l'encoder")
    startTime = time.time()
    global encoder
    encoder = LabelEncoder()
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Initialisation de l'encoder")

def labelEncoder(trainDf, testDf):
    cnf.logger.info("DEBUT : Encodage des labels")
    startTime = time.time()
    labels = trainDf.target.unique().tolist()
    labels.append("NEUTRAL")
    global encoder
    encoder.fit(trainDf.target.tolist())
    yTrain = encoder.transform(trainDf.target.tolist())
    yTest = encoder.transform(testDf.target.tolist())
    yTrain = yTrain.reshape(-1,1)
    yTest = yTest.reshape(-1,1)
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Encodage des labels")
    return yTrain, yTest