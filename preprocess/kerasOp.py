#################################################
### Keras                                     ###
#################################################

import time
from keras import utils
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Embedding
from config import config as cnf

tokenizer = None

def initTokenizer(trainDf):
    cnf.logger.info("DEBUT : Initialisation du tokenizer")
    startTime = time.time()
    global tokenizer
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(trainDf.text)
    vocabSize = len(tokenizer.word_index) + 1
    cnf.logger.debug("Taille du vocabulaire : {0}".format(str(vocabSize)))
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Initialisation du tokenizer")
    return vocabSize

def tokenizeText(trainDf, testDf):
    cnf.logger.info("DEBUT : Tokenisation du texte")
    startTime = time.time()
    global tokenizer
    xTrain = pad_sequences(tokenizer.texts_to_sequences(trainDf.text), maxlen = int(cnf.config["KERAS"]["sequenceLength"]))
    xTest = pad_sequences(tokenizer.texts_to_sequences(testDf.text), maxlen = int(cnf.config["KERAS"]["sequenceLength"]))
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Tokenisation du texte")
    return xTrain, xTest

def embeddingLayer(vocabSize, embeddingMatrix):
    cnf.logger.info("DEBUT : Création de la couche d'intégration du w2cModel")
    startTime = time.time()
    embeddingLayer = Embedding(
        vocabSize, 
        int(cnf.config["WORD2VEC"]["size"]), 
        weights = [embeddingMatrix], 
        input_length = int(cnf.config["KERAS"]["sequenceLength"]), 
        trainable = False
    )
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Création de la couche d'intégration du w2cModel")
    return embeddingLayer