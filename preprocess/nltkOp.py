#################################################
### Natural Language ToolKit                  ###
#################################################

import re
import time
import nltk
from nltk.corpus import stopwords
from  nltk.stem import SnowballStemmer
from config import config as cnf

def dlStopwords():
    cnf.logger.info("DEBUT : Téléchargement des stopwords")
    startTime = time.time()
    nltk.download("stopwords")
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Téléchargement des stopwords")

def getStopwords():
    return stopwords.words("english")

def getStemmer():
    return SnowballStemmer("english")

def preprocessText(text, stem = False):
    cnf.logger.debug("Preprocessing du texte : {0}".format(text))
    stopwords = getStopwords()
    stemmer = getStemmer()
    text = re.sub(cnf.config["TEXT_CLEANING"]["textCleaningRegex"], ' ', str(text).lower()).strip()
    tokens = []
    for token in text.split():
        if token not in stopwords:
            if stem:
                tokens.append(stemmer.stem(token))
            else:
                tokens.append(token)
    return " ".join(tokens)

def preprocessDataset(datasetDf):
    cnf.logger.info("DEBUT : Preprocessing du dataset")
    startTime = time.time()
    cnf.logger.debug("Nettoyage des données")
    df = datasetDf.text.apply(lambda x: preprocessText(x))
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Preprocessing du dataset")
    return df