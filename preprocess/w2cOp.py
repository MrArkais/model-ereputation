#################################################
### Word2Vec                                  ###
#################################################

import gensim
import time
import numpy as np
from config import config as cnf

w2cModel = None

def getDocuments(trainDf):
    cnf.logger.info("DEBUT : Split des mots de chaque ligne du dataset")
    startTime = time.time()
    documents = [_text.split() for _text in trainDf.text]
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Split des mots de chaque ligne du dataset")
    return documents

def createModel():
    cnf.logger.info("DEBUT : Création du w2cModel")
    startTime = time.time()
    global w2cModel
    w2cModel = gensim.models.word2vec.Word2Vec(
        size = int(cnf.config["WORD2VEC"]["size"]), 
        window = int(cnf.config["WORD2VEC"]["windows"]), 
        min_count = int(cnf.config["WORD2VEC"]["minCount"]),
        workers = 8
    )
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Création du w2cModel")

def buildVocab(documents):
    cnf.logger.info("DEBUT : Build du vocabulaire du w2cModel")
    startTime = time.time()
    global w2cModel
    w2cModel.build_vocab(documents)
    mots = w2cModel.wv.vocab.keys()
    vocabSize = len(mots)
    cnf.logger.debug("Taille du vocabulaire : {0}".format(str(vocabSize)))
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Build du vocabulaire du w2cModel")

def trainW2VModel(documents):
    cnf.logger.info("DEBUT : Entrainement du w2cModel")
    startTime = time.time()
    global w2cModel
    w2cModel.train(
        documents, 
        total_examples = len(documents), 
        epochs = int(cnf.config["WORD2VEC"]["epoch"])
    )
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Entrainement du w2cModel")

def embeddingMatrix(vocabSize, tokenizer):
    cnf.logger.info("DEBUT : Création de la matrice d'intégration du w2cModel")
    startTime = time.time()
    global w2cModel
    embeddingMatrix = np.zeros((vocabSize, int(cnf.config["WORD2VEC"]["size"])))
    for word, i in tokenizer.word_index.items():
        if word in w2cModel.wv:
            embeddingMatrix[i] = w2cModel.wv[word]
    cnf.logger.info("Durée d'éxecution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Création de la matrice d'intégration du w2cModel")
    return embeddingMatrix