import time
import pickle
from keras.models import Sequential
from keras.layers import Activation, Dense, Dropout, Flatten, Conv1D, MaxPooling1D, LSTM
from keras import utils
from keras.callbacks import ReduceLROnPlateau, EarlyStopping
from config import config as cnf

model = None

def buildModel(embeddingLayer):
    cnf.logger.info("DEBUT : Build du model")
    startTime = time.time()
    global model
    model = Sequential()
    model.add(embeddingLayer)
    model.add(Dropout(0.5))
    model.add(LSTM(100, dropout = 0.2, recurrent_dropout = 0.2))
    model.add(Dense(1, activation = "sigmoid"))
    model.summary(print_fn = lambda x: cnf.logger.info(x))
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Build du model")

def compileModel():
    cnf.logger.info("DEBUT : Compilation du model")
    startTime = time.time()
    global model
    model.compile(
        loss = "binary_crossentropy",
        optimizer = "adam",
        metrics = ["accuracy"]
    )
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Compilation du model")

def getCallbacks():
    cnf.logger.info("DEBUT : Récupération des callbacks")
    startTime = time.time()
    callbacks = [
        ReduceLROnPlateau(monitor = "val_loss", patience = 5, cooldown = 0),
        EarlyStopping(monitor = "val_accuracy", min_delta = 1e-4, patience = 5)
    ]
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Récupération des callbacks")
    return callbacks

def trainModel(xTrain, yTrain, callbacks):
    cnf.logger.info("DEBUT : Entrainement du model")
    startTime = time.time()
    global model
    trainResult = model.fit(
        xTrain, 
        yTrain,
        batch_size = int(cnf.config["KERAS"]["batchSize"]),
        epochs = int(cnf.config["KERAS"]["epoch"]),
        validation_split = 0.1,
        verbose = 1,
        callbacks = callbacks
    )
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Entrainement du model")
    return trainResult

def evaluateModel(xTest, yTest):
    cnf.logger.info("DEBUT : Evaluation du modele")
    startTime = time.time()
    global model
    score = model.evaluate(
        xTest, 
        yTest, 
        batch_size = int(cnf.config["KERAS"]["batchSize"])
    )
    cnf.logger.info("Précision : {0}".format(str(score[1])))
    cnf.logger.info("Perte : {0}".format(str(score[0])))
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Evaluation du modele")

def saveModel(tokenizer, encoder):
    cnf.logger.info("DEBUT : Enregistrement du modele")
    startTime = time.time()
    global model
    model.save("{0}/{1}".format(cnf.config["SAVE"]["path"], cnf.config["SAVE"]["modelName"]))
    pickle.dump(tokenizer, open("{0}/{1}".format(cnf.config["SAVE"]["path"], cnf.config["SAVE"]["tokenizerName"]), "wb"), protocol = 0)
    pickle.dump(encoder, open("{0}/{1}".format(cnf.config["SAVE"]["path"], cnf.config["SAVE"]["encoderName"]), "wb"), protocol = 0)
    cnf.logger.info("Durée d'execution : {0} secondes".format(time.time() - startTime))
    cnf.logger.info("FIN : Enregistrement du modele")

